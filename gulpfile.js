var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var fileinclude = require('gulp-file-include');
var bs = require('browser-sync').create();
var markdown = require('markdown');
var ftp = require('gulp-ftp');

var target = {
    sassSrc       : 'src/scss/styles.scss',
    sassWatchSrc  : 'src/scss/**/*.scss',
    sassWww       : 'app/css/',
    jsSrc         : 'src/js/*.js',
    jsWww         : 'app/js/',
    htmlSrc       : 'src/*.html',
    htmlWww       : 'app/',
    htmlWatchSrc  : 'src/**/*.html',
    mdWatch       : 'src/**/*.md',
    imgSrc        : 'src/img/**/*',
    imgWww        : 'app/img/'
};

gulp.task('sass', function() {
	return gulp.src(target.sassSrc)
		.pipe(sass())
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulp.dest(target.sassWww))
		.pipe(bs.stream())
});

gulp.task('html', function() {
  gulp.src(target.htmlSrc)
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file',
      filters: {
        markdown: markdown.parse
      }
    }))
    .pipe(gulp.dest(target.htmlWww))
	.pipe(bs.stream())
});

gulp.task('js', function () {
    gulp.src(target.jsSrc)
        .pipe(gulp.dest(target.jsWww))
        .pipe(bs.stream())
});

gulp.task('img', function () {
    gulp.src(target.imgSrc)
        .pipe(gulp.dest(target.imgWww))
        .pipe(bs.stream())
});

gulp.task('bs', function() {
	bs.init({
		server: {
			baseDir: "app/"
		}
	});
	gulp.watch(target.sassWatchSrc, ['sass']);
    gulp.watch(target.htmlSrc, ['html']);
	gulp.watch(target.htmlWatchSrc, ['html']);
    gulp.watch(target.jsSrc, ['js']);
    gulp.watch(target.mdWatch, ['html']);
});

gulp.task('ftpBOOK', function () {
    return gulp.src('app/**/*')
        .pipe(ftp({
            host: 'mstrs.ftp.ukraine.com.ua',
            user: 'mstrs_y4t6',
            pass: 'SwyrogLec18',
			remotePath: '/wtfjsbook'
        }))
		.pipe(bs.stream())
		.pipe(notify({message: 'FTP processed!'}));
});

gulp.task('default', function() {
    gulp.run('img', 'sass', 'html', 'js', 'bs');
});
