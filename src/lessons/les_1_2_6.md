Подробнее читайте [здесь](https://learn.javascript.ru/variable-names)

##### Правила именования

1. Никакого транслита. Только английский.
2. Использовать короткие имена только для переменных «местного значения».
3. Переменные из нескольких слов пишутся `togetherLikeThis`.
4. **Имя переменной должно максимально чётко соответствовать хранимым в ней данным**

##### Tasks

1. Объявление переменных [LJS](https://learn.javascript.ru/variable-names#объявление-переменных), [JSBIN](https://jsbin.com/mapirir/edit?html,js,output)
