Подробнее читайте [здесь](http://learn.javascript.ru/arguments-pseudoarray)

##### arguments – это не массив

Частая ошибка новичков – попытка применить методы Array к arguments. Это невозможно:

```function sayHi() {
    var a = arguments.shift(); // ошибка! нет такого метода!
}
sayHi(1);
```
